<?php
namespace App\Database;


use PDO;

class DBConnection
{
    private $host = "localhost";
    private $port = "3306";
    private $dbname = "vanilla";
    private $username = "root";
    private $password = "";
   
    public function connect(){
        $dsn = 'mysql:host=' . $this->host . ';port='. $this->port .';dbname=' . $this->dbname;
        $pdo = new PDO($dsn, $this->username, $this->password);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $pdo;
    }
}