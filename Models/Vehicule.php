<?php

namespace App\Models;

class Vehicule extends Model
{
    protected $id;
    protected $immatriculation;
    protected $dateCirculation;
    protected $kilometrage;
    protected $dateCT;
    protected $sanitaire;
    protected $etat;
    protected $dateKM;
    protected $modele;
    protected $surnom;
     
    public function __construct()
    {
        $this->table = 'vehicule';
    }

    function setId($id)
    {
        $this->id = $id;
        return $this;
    }
 
    function setImmatriculation($immatriculation)
    {
        $this->immatriculation = $immatriculation;
        return $this;
    }
 
    function setDateCirculation($dateCirculation)
    {
        $this->dateCirculation = $dateCirculation;
        return $this;
    }
 
    function setKilometrage($kilometrage)
    {
        $this->kilometrage = $kilometrage;
        return $this;
    }
 
    function setDateCT($dateCT)
    {
        $this->dateCT = $dateCT;
        return $this;
    }
    function setSanitaire($sanitaire)
    {
        $this->sanitaire = $sanitaire;
        return $this;
    }
    function setEtat($etat)
    {
        $this->etat = $etat;
        return $this;
    }
    function setDateKM($dateKM)
    {
        $this->dateKM = $dateKM;
        return $this;
    }
    function setSurnom($surnom)
    {
        $this->surnom = $surnom;
        return $this;
    }
     
     
     
     
    public function getId()
    {
        return $this->id;
    }
     
    public function getImmatriculation()
    {
        return $this->immatriculation;
    }
     
    public function getDateCirculation()
    {
        return $this->dateCirculation;
    }
     
    public function getKilometrage()
    {
        return $this->kilometrage;
    }
     
    public function getDateCT()
    {
        return $this->dateCT;
    }
    public function getSanitaire()
    {
        return $this->sanitaire;
    }
    public function getEtat()
    {
        return $this->etat;
    }
    public function getDateKM()
    {
        return $this->dateKM;
    }
    public function getSurnom()
    {
        return $this->surnom;
    }
     
     
}