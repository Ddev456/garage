<?php

namespace App\Models;

class Entretien extends Model
{
    protected $id;
    protected $intitule;
    protected $dateEntretien;
    protected $periodicite;
    protected $vehicule_id;
     
    public function __construct()
    {
        $this->table = 'entretien';
    }

    function setId($id)
    {
        $this->id = $id;
        return $this;
    }
 
    function setIntitule($intitule)
    {
        $this->intitule = $intitule;
        return $this;
    }
 
    function setDateEntretien($dateEntretien)
    {
        $this->dateEntretien = $dateEntretien;
        return $this;
    }
 
    function setPeriodicite($periodicite)
    {
        $this->periodicite = $periodicite;
        return $this;
    }
    function setVehiculeId($vehicule_id)
    {
        $this->vehicule_id = $vehicule_id;
        return $this;
    }
      
     
    public function getId()
    {
        return $this->id;
    }
     
    public function getIntitule()
    {
        return $this->intitule;
    }
     
    public function getDateEntretien()
    {
        return $this->dateEntretien;
    }
     
    public function getPeriodicite()
    {
        return $this->periodicite;
    }
    public function getVehiculeId()
    {
        return $this->vehicule_id;
    }
}