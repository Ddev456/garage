<?php
namespace App\Models;

class Users extends Model
{
    protected $id;
    protected $identifiant;
    protected $password;
    protected $roles;
    protected $token;
    protected $actif;

    public function __construct()
    {
        $class = str_replace(__NAMESPACE__.'\\', '', __CLASS__);
        // $this->table = strtolower(str_replace('Model', '', $class));
        $this->table = 'users';
    }

    /**
     * Récupérer un user à partir de son identifiant
     * @param string $identifiant
     * @return mixed 
     */
    public function findOneByIdentifiant($identifiant)
    {
        return $this->requete("SELECT * FROM {$this->table} WHERE identifiant = ?", [$identifiant])->fetch();
    }

    /**
     * Crée la session de l'utilisateur
     * @return void 
     */
    public function setSession()
    {
        $_SESSION['user'] = [
            'id' => $this->id,
            'identifiant' => $this->identifiant,
            'roles' => $this->roles
        ];
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getIdentifiant()
    {
        return $this->identifiant;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setIdentifiant($identifiant)
    {
        $this->identifiant = $identifiant;

        return $this;
    }

    /**
     * Get the value of password
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */ 
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }


    /**
     * Get the value of roles
     */ 
    public function getRoles()
    {
        $roles = $this->roles;

        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * Set the value of roles
     *
    //  * @return  self
     */ 
    public function setRoles($roles)
    {
        $this->roles = json_decode($roles);
        $this->roles = $roles;
        return $this;
    }
    public function getToken()
    {
        return $this->token;
    }
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }
    public function getActif()
    {
        return $this->actif;
    }
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }
}