<?php

namespace App\Tools;

class Form
{
    private $data;
    
    public function __construct($data = array()){
        $this->data = $data;
    }
    private function getValue($index){
        return isset($this->data[$index]) ? $this->data[$index] : null;
    }
    public function input($name, $type)
    {
        return '<input type="'. $type .'" name="'. $name .'" value="'. $this->getValue($name) .'">';
    }
    public function submit()
    {
        return '<button type="submit">Modifier</button>';
    }
}