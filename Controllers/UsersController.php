<?php
namespace App\Controllers;

use App\Core\Form;
use App\Models\Users;
use App\Database\DBConnection;

class UsersController extends Controller
{

    public function index($token = NULL)
    {
        $tokenvalid = new Users;
        $connection = new DBConnection();
        $res = $connection->connect();
        $query = $res->prepare("SELECT * from users WHERE token= ?");
        $query->execute(array($token));
        $userexist = $query->rowCount();
        $obj = $query->fetchAll();
        
        if($token){
        if($userexist == 1)
        {
            if($obj[0]['actif'] == 0){
                $this->render('users/newuser', compact('obj'));
            }else{
                $_SESSION['erreur'] = 'Cette page n\'existe pas !';
                header('Location: /');
                exit;
            }
        }else{
            $_SESSION['erreur'] = 'Cette page n\'existe pas !';
            header('Location: /');
            exit;
            $this->render('users/index');
            }
        }else{
            $this->render('users/index');
        }   
    }
    /**
     * Connexion des utilisateurs
     */
    public function login(){
        // On vérifie si le formulaire est complet
        if(Form::validate($_POST, ['identifiant', 'password'])){
            // Le formulaire est complet
            // On va chercher dans la base de données l'utilisateur avec l'identifiant entré
            

            $usersModel = new Users();
            $userArray = $usersModel->findOneByIdentifiant(strip_tags($_POST['identifiant']));
   


            //  Si l'utilisateur n'existe pas
            if(!$userArray){
                // On envoie un message de session
                $_SESSION['erreur'] = 'L\'identifiant et/ou le mot de passe sont incorrects';
                header('Location: /users');
                exit;
            }
            // var_dump($userArray);
            // L'utilisateur existe
            
            
            $user = $usersModel->hydrate($userArray);

            // On vérifie si le mot de passe est correct
            // if($_POST['password'] == 'demo'){
            if($_POST['password'] !== $_POST['confirm_password']){
                $_SESSION['erreur'] = 'Les mots de passe ne correspondent pas !';
                header('Location: /users');
                exit;
            }else{
                if(password_verify($_POST['password'], $user->getPassword())){
                // Le mot de passe est bon
                // On crée la session
                $user->setSession();     
                header('Location: /');
                exit;
               
            }else{
                // Mauvais mot de passe
                $_SESSION['erreur'] = 'L\'identifiant et/ou le mot de passe sont incorrects';
                header('Location: /users');
                exit; 
                }
            }
        }else{
            echo 'erreur formulaire incomplet !';
        }
       
    }

    public function nouveau()
    {
        $id = strip_tags($_POST['user_id']);
        $identifiant = strip_tags($_POST['identifiant']);
        $pass = strip_tags($_POST['password']);
        $pswd = password_hash($pass, PASSWORD_DEFAULT);
        $newUser = new Users;
        $newUser->setId($id);
        $newUser->setIdentifiant($identifiant);
        $newUser->setPassword($pswd);
        $newUser->setActif(1);
        $newUser->update();

        $_SESSION['message'] = 'Votre compte a bien été validé !';
        header('Location: /');
        exit;
    }

    /**
     * Inscription des utilisateurs
     * @return void 
     */
    public function register()
    {
        $userModel = new Users();
        $users = $userModel->findAll();
        if($this->isAdmin()){
            $this->render('users/register', compact('users'));           
        }
    }

    public function changepswd($id)
    {
        $userModel = new Users();
        $users = $userModel->find($id);
        // if($this->isAdmin()){
            $this->render('users/changepswd', compact('users'));           
        // }
    }

    public function create()
    {
        $email = strip_tags($_POST['email']);
        $roles = strip_tags($_POST['roles']);
        // $roles = "admin";
        $randomtoken = random_bytes(5);
        $token = bin2hex($randomtoken);
        // $token = "slmfgjs";
        $actif = 0;
        $userModif = new Users;
        $connection = new DBConnection();
        $res = $connection->connect();
        $sql = "INSERT INTO users (roles, actif, token) VALUES (? ,? ,?)";
        $query = $res->prepare($sql);
        // $sql->execute($roles, $actif, $token);
        $query->bindParam(1, $roles);
        $query->bindParam(2, $actif);
        $query->bindParam(3, $token);
        var_dump($sql);
            $query->execute();
        $message = 'Bienvenue sur le service Authentification du CH de Laon,
 
        Pour activer votre compte, veuillez cliquer sur le lien ci-dessous
        ou copier/coller dans votre navigateur Internet.
 
        localhost/users/index/'.$token.'
 
 
        ---------------
        Ceci est un mail automatique, Merci de ne pas y répondre.';
        $mail = mail($email, 'Inscription au service d\'authentification CH Laon', $message, 'From: webmaster@example.com');

            header('Location: /');
             exit;
    }

    public function changeRoles()
    {
        $id = strip_tags($_POST['id']);
        $roles = strip_tags($_POST['roles2']);

        $userModif = new Users;
        $userModif->setId($id);
        $userModif->setRoles($roles);

        $userModif->update();
        $_SESSION['message'] = 'Le rôle a été mis à jour avec succès !';
        header('Location: /');
        exit;
    }

    public function manage()
    {
        // On se protège contre les failles XSS
        if($_POST['password'] !== $_POST['confirm_password']){
            $_SESSION['erreur'] = 'Les mots de passe ne correspondent pas !';
            header('Location: users/update');
        }
        $id = strip_tags($_POST['id']);
 
        // $identifiant = strip_tags($_POST['identifiant2']);
        $motdepasse = strip_tags($_POST['password']);
        // $roles = strip_tags($_POST['roles2']);
        $pswd = password_hash($motdepasse, PASSWORD_DEFAULT);
     //   on stocke l'utilisateur
        $userModif = new Users;
     // On hydrate
     
     $userModif->setId($id);
    // $userModif->setIdentifiant($identifiant);
    $userModif->setPassword($pswd);
    // $userModif->setRoles($roles);

     // On met à jour le véhicule
        $userModif->update();
        // var_dump($userModif);
        $_SESSION['message'] = 'Votre mot de passe a été mis à jour avec succès !';
        header('Location: /');
        exit;
     }
    

    /**
     * Déconnexion de l'utilisateur
     * @return exit 
     */
    public function logout(){
        unset($_SESSION['user']);
        header('Location: /');
        exit;
    }

}