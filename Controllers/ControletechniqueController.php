<?php

namespace App\Controllers;

use App\Core\Form;
use App\Models\Vehicule;
use App\Database\DBConnection;

class ControletechniqueController extends Controller
{
    public function index()
    {
        $vehiculeModel = new Vehicule();
        $vehicules = $vehiculeModel->findAll();
        if($this->isAdmin()){
            $this->render('controletechnique/index', compact('vehicules'));           
        }
    }
}