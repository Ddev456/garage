<?php
namespace App\Controllers;

abstract class Controller
{
    public function render($fichier, $donnees = [], $template = 'home')
    {
        // On extrait le contenu de $donnees
        extract($donnees);

        // On démarre le buffer de sortie
        ob_start();
        // A partir de ce point toute sortie est conservée en mémoire

        // On crée le chemin vers la vue
        require_once ROOT.'/Views/'.$fichier.'.php';

        // Transfère le buffer dans $contenu
        
        $contenu = ob_get_clean();

        // Template de page
        require_once ROOT.'/Views/'.$template.'.php';
    }
    /**
     * Vérifie si on est admin
     * @return true 
     */
    protected function isAdmin()
    {
        // On vérifie si on est connecté et si "ROLE_ADMIN" est dans nos rôles
        if(isset($_SESSION['user']) && $_SESSION['user']['roles'] == "admin"){
            // On est admin
            return true;
        }else{
            // On n'est pas admin
            $_SESSION['erreur'] = "Vous n'avez pas accès à cette zone";
            header('Location: /');
            exit;
        }
    }
}
