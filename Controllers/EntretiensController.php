<?php

namespace App\Controllers;

use App\Core\Form;
use App\Models\Entretien;
use App\Models\Vehicule;
use App\Database\DBConnection;

class EntretiensController extends Controller
{
    public function index()
    {
        $entretienModel = new Entretien();
        $entretiens = $entretienModel->findAll();
        if($this->isAdmin()){
            $this->render('entretiens/index', compact('entretiens'));           
        }
    }
    
public function add()
{
    $vehiculeModel = new Vehicule();

    $connection = new DBConnection();
        $res = $connection->connect();
        $sql = "SELECT id, surnom, immatriculation, kilometrage FROM vehicule";
        var_dump($sql);
        $query = $res->prepare($sql);
            $query->execute();
            $vehicules=$query->fetchAll();
            if($this->isAdmin()){
                $this->render('entretiens/add', compact('vehicules'));               
            }    
}

public function insert()
{
// On vérifie si l'utilisateur est connecté
//  ???? verifier Form::validate
if(isset($_SESSION['user']) && !empty($_SESSION['user']['id'])){
// L'utilisateur est connecté
// On vérifie si le formulaire est complet
if(Form::validate($_POST, ['intitule', 'dateEntretien', 'periodicite', 'vehicule_id'])){
   // Le formulaire est complet
   // On se protège contre les failles XSS
   // strip_tags, htmlentities, htmlspecialchars
   $intitule = strip_tags($_POST['intitule']);
   $dateEntretien = strip_tags($_POST['dateEntretien']);
   $periodicite = strip_tags($_POST['periodicite']);
    $vehicule_id = strip_tags($_POST['vehicule_id']);
    
   // On instancie notre modèle
   $entretien = new Entretien();

   // On hydrate
   $entretien->setIntitule($intitule);
   $entretien->setDateEntretien($dateEntretien);
   $entretien->setperiodicite($periodicite);
   $entretien->setVehiculeId($vehicule_id);
   
   // On enregistre
   $entretien->create();

   // On redirige
   var_dump($_POST);
   $_SESSION['message'] = "L'entretien a été enregistré avec succès";
   header('Location: /entretiens/index');
   exit;
}else{
   // Le formulaire est incomplet
   $_SESSION['erreur'] = !empty($_POST) ? "Veuillez compléter tous les champs" : '';
}

$this->render('entretiens/index');
}else{
// L'utilisateur n'est pas connecté
$_SESSION['erreur'] = "Vous devez être connecté(e) pour accéder à cette page";
header('Location: /users/login');
exit;
}
}

    /**
     * Modifier un entretien
     * @param int $id 
     * @return void 
     */
   public function modifier($id, $idEntretien)
   {
    $entretienModel = new Entretien();
    // On cherche l'annonce avec l'id $id
    // $entretiens = $entretienModel->find($id);
    $connection = new DBConnection();
        $res = $connection->connect();
        $sql = "SELECT surnom, immatriculation, E.id, libelle, periodicite, dateEntretien FROM entretien E LEFT JOIN vehicule V ON E.vehicule_id = V.id WHERE E.vehicule_id = $id AND E.id= $idEntretien";
        $query = $res->prepare($sql);
            $query->execute();
            $entretiens=$query->fetchAll();
            if($this->isAdmin()){
                $this->render('entretiens/modifier', compact('entretiens'));           
            }
    // Si l'annonce n'existe pas, on retourne à la liste des annonces  
   }

   public function update()
   {
       var_dump($_POST);
       // On se protège contre les failles XSS
    //    $id = strip_tags($_POST['vehicule_id']);
       $id = strip_tags($_POST['id']);

        $intitule = $_POST['intitule'];
        $dateEntretien = strip_tags($_POST['dateEntretien']);
        $dateEntretien1 = strtotime($dateEntretien);
        $dateEntretien2 = date('Y-m-d H:i:s', $dateEntretien1);
        $periodicite = strip_tags($_POST['periodicite']);
        $vehicule_id = ($_POST['vehicule_id']);

    //   on stocke le vehicule
       $entretienModif = new Entretien;
    // On hydrate
    $entretienModif->setId($id);
    $entretienModif->setIntitule($intitule);
    $entretienModif->setDateEntretien($dateEntretien2);
    $entretienModif->setPeriodicite($periodicite);
    $entretienModif->setVehiculeId($vehicule_id);
    

    
    // On met à jour le véhicule
       $entretienModif->update();
   
       header('Location: /admin');
       exit;
    }
    public function delete($id)
    {
      $entretienModel = new Entretien();
      $entretiens = $entretienModel->delete($id);
      header('Location: /entretiens/index');
       exit;
    }
}