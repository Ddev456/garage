<?php

namespace App\Controllers;
use App\Core\Form;
use App\Models\Vehicule;


class VehiculesController extends Controller
{
    public function index()
    {
        $vehiculeModel = new Vehicule();
        $vehicules = $vehiculeModel->findAll();
        $this->render('vehicules/index', compact('vehicules'));
} 
 
   public function card($id)
   {
      $vehiculeModel = new vehicule();
      $vehicules = $vehiculeModel->find($id);
      $this->render('vehicules/card', compact('vehicules'), 'home');
   }
 
   public function kilometrage() 
{
   $this->render('vehicules/kilometrage', [],'home');
}
    public function add()
    {
     $this->render('vehicules/add');     
    }

public function insert()
{
 // On vérifie si l'utilisateur est connecté
//  ???? verifier Form::validate
 if(isset($_SESSION['user']) && !empty($_SESSION['user']['id'])){
   // L'utilisateur est connecté
   // On vérifie si le formulaire est complet
   if(Form::validate($_POST, ['immatriculation', 'dateCirculation', 'kilometrage', 'dateCT', 'sanitaire', 'etat', 'dateKM', 'surnom'])){
       // Le formulaire est complet
       // On se protège contre les failles XSS
       // strip_tags, htmlentities, htmlspecialchars
       $surnom = strip_tags($_POST['surnom']);
       $immatriculation = strip_tags($_POST['immatriculation']);
       $dateCirculation = strip_tags($_POST['dateCirculation']);
        $kilometrage = strip_tags($_POST['kilometrage']);
        $dateCT = strip_tags($_POST['dateCT']);
        $sanitaire = strip_tags($_POST['sanitaire']);
        $etat = strip_tags($_POST['etat']);
        $dateKM = strip_tags($_POST['dateKM']);
        $dateKM1 = strtotime($dateKM);
        $dateKM2 = date('Y-m-d H:i:s', $dateKM1);
       // On instancie notre modèle
       $vehicule = new Vehicule();

       // On hydrate
       $vehicule->setImmatriculation($immatriculation);
       $vehicule->setDateCirculation($dateCirculation);
       $vehicule->setKilometrage($kilometrage);
       $vehicule->setDateCT($dateCT);
       $vehicule->setSanitaire($sanitaire);
       $vehicule->setEtat($etat);
       $vehicule->setDateKM($dateKM2);
       $vehicule->setSurnom($surnom);
       // On enregistre
       $vehicule->create();

       // On redirige
       var_dump($_POST);
       $_SESSION['message'] = "Le véhicule a été enregistré avec succès";
       header('Location: /vehicules/index');
       exit;
   }else{
       // Le formulaire est incomplet
       $_SESSION['erreur'] = !empty($_POST) ? "Veuillez compléter tous les champs" : '';
   }

   $this->render('vehicules/index');
}else{
   // L'utilisateur n'est pas connecté
   $_SESSION['erreur'] = "Vous devez être connecté(e) pour accéder à cette page";
   header('Location: /users/login');
   exit;
}
}

    /**
     * Modifier une annonce
     * @param int $id 
     * @return void 
     */
   public function modifier($id)
   {

    $vehiculeModel = new Vehicule();

    // On cherche l'annonce avec l'id $id
    $vehicules = $vehiculeModel->find($id);
    $this->render('vehicules/modifier', compact('vehicules'));
    // Si l'annonce n'existe pas, on retourne à la liste des annonces
    
   }

   public function update()
   {
       var_dump($_POST);
       // On se protège contre les failles XSS
    //    $id = strip_tags($_POST['vehicule_id']);
       $id = strip_tags($_POST['id']);

    $immatriculation = $_POST['immatriculation'];
       $dateCirculation = strip_tags($_POST['dateCirculation']);
        $kilometrage = strip_tags($_POST['kilometrage']);
        $dateCT = strip_tags($_POST['dateCT']);
        $sanitaire = strip_tags($_POST['sanitaire']);
        $etat = strip_tags($_POST['etat']);
        $dateKM = strip_tags($_POST['dateKM']);
        $dateKM1 = strtotime($dateKM);
        $dateKM2 = date('Y-m-d H:i:s', $dateKM1);
        $surnom = strip_tags($_POST['surnom']);
    //   on stocke le vehicule
       $vehiculeModif = new Vehicule;
    // On hydrate
    $vehiculeModif->setId($id);
    $vehiculeModif->setImmatriculation($immatriculation);
    $vehiculeModif->setDateCirculation($dateCirculation);
    $vehiculeModif->setKilometrage($kilometrage);
    $vehiculeModif->setDateCT($dateCT);
    $vehiculeModif->setSanitaire($sanitaire);
    $vehiculeModif->setEtat($etat);
    $vehiculeModif->setDateKM($dateKM2);
    $vehiculeModif->setSurnom($surnom);
    
    // On met à jour le véhicule
       $vehiculeModif->update();
   
       header('Location: /');
       exit;
    }

    public function delete($id)
    {
      $vehiculeModel = new Vehicule();
      $vehicules = $vehiculeModel->delete($id);
      header('Location: /vehicules/index');
       exit;
    }
   
}
