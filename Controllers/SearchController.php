<?php

namespace App\Controllers;



class SearchController extends Controller{

public function index()
    {
    $this->render('vehicules/search', [], 'ajax');
    }

public function method()
    {
    $this->render('entretiens/EntretiensSearch', [], 'ajax');
    }

public function select()
    {
    $this->render('controletechnique/CTSearch', [], 'ajax');
    }
public function archives()
    {
    $this->render('archives/ArchivesSearch', [], 'ajax');
    }

}


