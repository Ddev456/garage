<?php

namespace App\Controllers;

use App\Models\Vehicule;
use App\Database\DBConnection;

class VehiculesArchivesController extends Controller
{
    public function index()
    {
        $vehiculeModel = new Vehicule();
        $connection = new DBConnection();
        $res = $connection->connect();
        $sql = "SELECT surnom, immatriculation, kilometrage FROM vehicule WHERE etat = 2 OR etat = 3";
        $query = $res->prepare($sql);
            $query->execute();
            $vehicules=$query->fetchAll();
            if($this->isAdmin()){           
                $this->render('vehiculesArchives/index', compact('vehicules'));
            }
    }
}