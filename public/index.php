<?php
use App\Autoloader;
use App\Core\Router;

// On définit une constante contenant le dossier racine du projet
define('ROOT', dirname(__DIR__));

// On importe l'autoloader
require_once ROOT.'/Autoloader.php';
Autoloader::register();

// On instancie Routeur
$app = new Router();

// On démarre l'application
$app->start();