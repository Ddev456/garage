-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 28 Juillet 2020 à 06:35
-- Version du serveur :  5.7.11
-- Version de PHP :  7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `vanilla`
--

-- --------------------------------------------------------

--
-- Structure de la table `entretiens`
--

CREATE TABLE `entretiens` (
  `id` int(11) NOT NULL,
  `intitule` varchar(255) NOT NULL,
  `dateEntretien` date NOT NULL,
  `periodicite` int(11) NOT NULL,
  `vehicule_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `entretiens`
--

INSERT INTO `entretiens` (`id`, `intitule`, `dateEntretien`, `periodicite`, `vehicule_id`) VALUES
(1, 'Vidange', '2015-07-02', 15000, 6),
(2, 'Changement de Pneus', '2018-02-14', 80000, 2),
(3, 'Vidange', '2020-07-07', 35000, 9);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `identifiant` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `roles` longtext NOT NULL,
  `actif` int(11) NOT NULL,
  `token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `identifiant`, `password`, `roles`, `actif`, `token`) VALUES
(5, 'demo', '$2y$10$m00w1Lu2kT9O79JymOIyB.LGcsATfwJFY9nrhxzfNSiNkbwfxjkAq', 'admin', 1, ''),
(14, 'Ambulancier', '$2y$10$KHjcQPeAH3FPbGfRKEeS8uY9CP0.rqh0xF/icyr4EzSHZGYYCTOG6', 'user', 1, '14c78e083c');

-- --------------------------------------------------------

--
-- Structure de la table `vehicules`
--

CREATE TABLE `vehicules` (
  `id` int(11) NOT NULL,
  `immatriculation` varchar(255) NOT NULL,
  `dateCirculation` date NOT NULL,
  `kilometrage` int(11) NOT NULL,
  `dateCT` date NOT NULL,
  `sanitaire` tinyint(1) NOT NULL,
  `etat` tinyint(1) NOT NULL,
  `dateKM` datetime DEFAULT NULL,
  `surnom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `vehicules`
--

INSERT INTO `vehicules` (`id`, `immatriculation`, `dateCirculation`, `kilometrage`, `dateCT`, `sanitaire`, `etat`, `dateKM`, `surnom`) VALUES
(1, 'BB-457-YZ', '2014-07-01', 145210, '2020-07-22', 0, 1, '1970-01-01 00:00:00', 'Volskwagen Golf'),
(2, 'YY-213-LL', '2018-08-09', 100221, '2020-07-30', 0, 0, '1970-01-01 00:00:00', 'Renault Kangoo'),
(3, 'XX-654-TY', '2017-07-01', 120456, '2021-01-05', 0, 0, NULL, 'Renault Clio'),
(5, 'TT-120-AZ', '2019-11-04', 45879, '2020-06-05', 0, 0, NULL, 'Renault Twingo'),
(6, 'EE-450-UO', '2013-01-10', 120546, '2020-08-08', 0, 1, '2020-07-23 00:00:00', 'Opel Corsa'),
(7, 'DD-435-RE', '2018-02-01', 42150, '2020-10-18', 0, 1, '2020-07-23 00:00:00', 'Audi A4'),
(9, 'AA-660-TF', '2012-09-07', 154550, '2020-08-02', 0, 1, '2020-07-23 00:00:00', 'Fiat Punto');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `entretiens`
--
ALTER TABLE `entretiens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicule_id` (`vehicule_id`),
  ADD KEY `vehicule_id_2` (`vehicule_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vehicules`
--
ALTER TABLE `vehicules`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `entretiens`
--
ALTER TABLE `entretiens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `vehicules`
--
ALTER TABLE `vehicules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `entretiens`
--
ALTER TABLE `entretiens`
  ADD CONSTRAINT `entretiens_ibfk_1` FOREIGN KEY (`vehicule_id`) REFERENCES `vehicules` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
