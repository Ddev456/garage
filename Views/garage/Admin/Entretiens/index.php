<?php

// use App\Controllers\VehiculeController;

use App\Controllers\EntretiensController;
// use App\Repository\EntretiensRepository;

$donnees = new EntretiensController();
$ent = $donnees->showAllEntretiens();

?>
<div class="container">
<h1>Gérer les Entretiens</h1>

    <table class="table table-stripped">
        <thead>
            <tr>
                <th>Surnom Véhicule</th>
                <th>Intitulé de l'entretien</th>
                <th>Date de l'entretien</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
          <?php  foreach($ent as $entretien){ ?>
            <tr>
                <td><?= $entretien['surnom'] ?></td>
                <td><?= $entretien['intitule'] ?></td>
                <td><?= date('d-m-Y', strtotime($entretien['dateEntretien'])) ?></td>
                <td>
                    <a href="/entretiens/edit?id=<?= $entretien['id'] ?>" class="btn btn-secondary">Editer</a>
                    <input type="hidden" name="editId" value="<?= $entretien['id'] ?>">
                </td>
                <td>
                    <form action="/entretiens" method="POST">
                    <input type="hidden" name="_delete" value="<?= $entretien['id'] ?>">
                    <button type="submit" name="delete" class="btn btn-danger">Supprimer</button>
                    </form>
                </td>
            </tr>
          <?php } ?>
        </tbody>
    </table>
    <div class="text-right">
        <a href="/entretiens/add" class="btn btn-primary">Ajouter un Entretien</a>
    </div>
</div>


