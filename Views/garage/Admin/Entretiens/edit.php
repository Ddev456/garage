<?php

use App\Controllers\EntretiensController;
use App\Repository\EntretiensRepository;

$donnees = new EntretiensController();
$ent = $donnees->showEntretiensById($_GET['id']);

if(isset($_POST['goEdit']))
{
    $intitule = $_POST['intitule'];
    $dateEntretien1 = strtotime($_POST['dateEntretien']);
    $dateEntretien = date('Y-m-d', $dateEntretien1);
    $periodicite = $_POST['periodicite'];

    $update = new EntretiensRepository();
    $veh2 = $update->edit($_GET['id'], $intitule, $dateEntretien, $periodicite);
    if($veh2){
        echo 'Modifié avec succès! ';
    }
}

?>

<?php  foreach($ent as $entretien){ ?>
<div class="container mt-4">
    <h1>Editer l'entretien concernant le véhicule : <?= $entretien['surnom'] ?> immatriculé  <?= $entretien['immatriculation'] ?></h1>

    <form action="/entretiens/edit?id=<?= $_GET['id'] ?>" name="editEntretien" method="POST">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="intitule">Intitulé de l'entretien</label>
                    <input type="text" name="intitule" id="intitule" class="form-control" value="<?= $entretien['intitule'] ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="dateEntretien">Date de l'entretien</label>
                    <input type="date" name="dateEntretien" id="dateEntretien" class="form-control" value="<?= $entretien['dateEntretien'] ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="periodicite">Périodicité</label>
                    <input type="number" name="periodicite" id="periodicite" class="form-control" value="<?= $entretien['periodicite'] ?>">
                </div>
            </div>
        </div>


       

        <button type="submit" name="goEdit" class="btn btn-primary">Modifier</button>
        <?php } ?>
    </form>


</div>


