<h1 class="text-center">Service d'authentification</h1>

<h4>Veuillez créer votre identifiant ainsi que votre mot de passe</h4>
<?php foreach($obj as $item){ ?>
    <!-- <img src="logo.jpg" class="img" alt="" style="display:block; margin-top:1rem; margin-left:auto; margin-right:auto;"> -->
    <form class="form-horizontal" action="/users/new/" method="POST">
    <div class="row">
        <input type="hidden" name="user_id" value="<?= $item['id'] ?>">
<?php } ?>
        <div class="col-sm-6 offset-sm-3">
        <img src="logoCH.png" alt="">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-user fa-fw"></i></span>
                </div>
                <input type="text" class="form-control" name="identifiant" placeholder="Entrez votre identifiant" aria-label="Username" aria-describedby="basic-addon1" required>
            </div>

        </div>

        <div class="col-sm-6 offset-sm-3">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-key fa-fw"></i></span>
                </div>
                <input type="password" class="form-control" name="password2" placeholder="Entrez votre mot de passe" aria-label="Username" aria-describedby="basic-addon1" required>
            </div>
        </div>

        <div class="col-sm-6 offset-sm-3">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-key fa-fw"></i></span>
                </div>
                <input type="password" class="form-control" name="password" placeholder="Confirmer le mot de passe" aria-label="Username" aria-describedby="basic-addon1" required>
            </div>
        </div>
        <div class="col-sm-6 offset-sm-5">
            <button id="btn_submit" name="btn_submit" class="btn btn-primary" style="margin-left:auto; margin-right:auto;" value="connexion">Connexion</button>
        </div>
      

</div>


</form>

</div>


</div>