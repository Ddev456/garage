<div class="container mt-4">
<h1>Gestion des utilisateurs</h1>

<form action="/users/create" method="POST">
<h4>Ajouter un utilisateur</h4>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                <label for="email">E-mail:</label>
                <input class="form-control" type="email" name="email" id="email">
                </div>
            </div>
            <!-- <div class="col-md-3"> -->
                <!-- <div class="form-group">
            <label for="password">Mot de passe:</label>
                <input class="form-control" type="password" name="password" id="password">
            </div> -->
            <!-- </div> -->
            <div class="col-md-3">
            <div class="form-group">
            <label for="role">Rôle:</label>
            <select class="form-control" name="roles" id="roles">
                <option value="admin">Admin</option>
                <option value="user">Ambulancier</option>
            </select>
        </div>
            </div>
            <div class="col-md-3">
            <button style="margin-top: 30px;" class="btn btn-primary" type="submit">Valider</button>
            </div>
       
        </div>
        </form>


<h4>Gérer les utilisateurs</h4>
<div class="jumbotron">
<!-- <div class="alert alert-danger" role="alert">
  Merci d'indiquer le mot de passe utilisateur avant de continuer ! 
</div> -->


    <?php foreach($users as $user)
    {
        ?> 
        <form action="/users/changeRoles" method="POST"> 
        <div class="row">
        <div class="col-md-3">
                <div class="form-group">
                    <input type="hidden" name="id" value="<?= $user['id'] ?>">
                <label for="identifiant2">Identifiant :</label>
                <input class="form-control" type="text" name="identifiant2" value="<?= $user['identifiant'] ?>" disabled="disabled">
            </div>
            </div>
            <!-- <div class="col-md-3">
                <div class="form-group">
            <label for="password2">Mot de passe :</label>
                <input class="form-control" type="password2" name="password">
            </div>
            </div> -->
            <div class="col-md-3">
            <div class="form-group">
            <label for="roles2">Rôle :</label>
            <select class="form-control" name="roles2" required>
                <option value="">Sélectionner le rôle</option>
                <option value="admin">Admin</option>
                <option value="user">Ambulancier</option>
            </select>
        </div>
            </div>
            <div class="col-md-3">
            <button style="margin-top: 30px;" class="btn btn-danger" type="submit">Modifier</button>
            </div>
            </div>
            </form>
     <?php   }  ?>
        
        </div>
       
</div>