<!DOCTYPE html>
<html lang="en">
<head>
  <title>Live Search</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
  <h1>Rechercher un Véhicule dans la base de données</h1>
</div>
  
<div class="container">
  <div class="row">
    <!-- <div class="col-sm-3">
    </div> -->
    <div class="col-sm-6">
    <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-search fa-fw"></i></span>
                </div>
                <input type="text" class="form-control" id="search" placeholder="Rechercher un véhicule" aria-label="Username" aria-describedby="basic-addon1" required>
            </div>
    <!-- <div class="col-sm-3"> -->
    <a href="/vehicules/add" class="btn btn-success"><i class="fa fa-plus"></i> Ajouter un véhicule</a>
    <!-- </div> -->
      <table class="table table-hover">
      <thead>
        <tr>
          <th>Nom du véhicule</th>
          <th>Immatriculation</th>
          <th>Kilométrage</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody id="output">
        
      </tbody>
    </table>
    </div>
  </div>
</div>
<script type="text/javascript">

function AJAX(){
  $.ajax({
        type:'POST',
        url: '/search',
        data:{
          name:$("#search").val(),
        },
        success:function(data){
          $("#output").html(data);
        }
      });
}

  $(document).ready(function(){
    AJAX();
    $("#search").keyup(function(){
      AJAX();
      
    });
  });

</script>
<!-- <?php
// Create connection
$conn = mysqli_connect("localhost", "root", "root", "vanilla");

if($_POST == ''){
  $sql = "SELECT * FROM vehicules";
}




if(isset($_POST['name'])){

$sql = "SELECT * FROM vehicules WHERE surnom LIKE '%".$_POST['name']."%'";
$result = mysqli_query($conn, $sql);
if(mysqli_num_rows($result)>0){
	while ($row=mysqli_fetch_assoc($result)) {
		echo "	<tr>
		          <td>".$row['surnom']."</td>
		          <td>".$row['immatriculation']."</td>
		          <td>".$row['kilometrage']."km</td>
		        </tr>";
	}
}
else{
	echo "<tr><td>0 result's found</td></tr>";
}
}
?> -->

</body>
</html>
