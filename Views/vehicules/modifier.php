<h1>Modifier un véhicule</h1>
<?php


?>

<div class="container mt-4">
    <h1>Editer le Véhicule</h1>

    <form action="/vehicules/update/" name="editVehicule" method="POST">

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="immatriculation">Immatriculation</label>
                    <input type="text" name="immatriculation" id="immatriculation" class="form-control" value="<?= $vehicules['immatriculation'] ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="dateCirculation">Date de mise en circulation</label>
                    <input type="date" name="dateCirculation" id="dateCirculation" class="form-control" value="<?= $vehicules['dateCirculation'] ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="kilometrage">Kilométrage au compteur</label>
                    <input type="number" name="kilometrage" id="kilometrage" class="form-control" value="<?= $vehicules['kilometrage'] ?>">
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="dateCT">Date Contrôle technique</label>
                    <input type="date" name="dateCT" id="dateCT" class="form-control" value="<?= $vehicules['dateCT'] ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class="form-check">
                        <label class="form-check-label" for="oui">Sanitaire</label>
                        <input type="radio" name="sanitaire" id="oui" class="form-control" value="1" <?php if($vehicules['sanitaire'] == 1){ echo 'checked'; } ?>>
                        <label class="form-check-label" for="non">Non sanitaire</label>
                        <input type="radio" id="non" name="sanitaire" class="form-control" value="0"<?php if($vehicules['sanitaire'] == 0){ echo 'checked'; } ?>>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                <div class="form-check">
                        <label class="form-check-label" for="vendu">Vendu</label>
                        <input type="radio" name="etat" id="vendu" class="form-control" value="1" <?php if($vehicules['etat'] == 1){ echo 'checked'; } ?>>
                        <label class="form-check-label" for="parc">Parc</label>
                        <input type="radio" id="parc" name="etat" class="form-control" value="0"<?php if($vehicules['etat'] == 0){ echo 'checked'; } ?>>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="dateKM">Date MAJ Kilométrage</label>
                    <input type="date" name="dateKM" id="dateKM" class="form-control" value="<?= $vehicules['dateKM'] ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class="form-check">
                        <label class="form-check-label" for="surnom">Nom du véhicule</label>
                        <input type="text" name="surnom" id="surnom" class="form-control" value="<?= $vehicules['surnom'] ?>">
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="id" value="<?= $vehicules['id'] ?>">
        <!-- <a href="/vehicules/update">Modifier</a> -->
        <button type="submit" name="updatevehicule" class="btn btn-primary">Modifier</button>

    </form>


</div>
