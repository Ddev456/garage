<?php

use App\Controllers\VehiculeController;
use App\Repository\VehiculeRepository;

$donnees = new VehiculeController();
$veh = $donnees->showVehiculeById($_GET['id']);

// ne pas mettre le nom du form mais du submit !
if(isset($_POST['goEdit']))
{
    $immatriculation = $_POST['vehicule_immatriculation'];
    // $dateCirculation = date_create_from_format('Y/M/d', $_POST['vehicule_dateCirculation']);
    // $dateCirculation->getTimestamp();
    $dateCirculation1 = strtotime($_POST['vehicule_dateCirculation']);
    $dateCirculation = date('Y-m-d', $dateCirculation1);
    // $dateCirculation1 = $_POST['vehicule_dateCirculation'];
    $kilometrage = $_POST['vehicule_kilometrage'];
    // $dateCT = date_create_from_format('Y/M/d', $_POST['vehicule_dateCT']);
    // $dateCT->getTimestamp();
    $dateCT1 = strtotime($_POST['vehicule_dateCT']);
    $dateCT = date('Y-m-d', $dateCT1);
    // $dateCT = $_POST['vehicule_dateCT'];
    $sanitaire = $_POST['sanitaire'];
    $etat = $_POST['etat'];
    // $dateKM = new DateTime($_POST['vehicule_dateKM']);
    // $dateKM->format('Y-m-d H:i:s');
    $dateKM1 = strtotime($_POST['vehicule_dateKM']);
    $dateKM = date('Y-m-d H:i:s', $dateKM1);
    // $dateKM = $_POST['vehicule_dateKM'];
    $surnom = $_POST['vehicule_surnom'];
    // $array = ['immatriculation', 'dateCirculation', ':kilometrage', 'dateCT', 'sanitaire', 'etat', 'dateKM', 'surnom'];
    //     foreach($array as $item){
    //         if(isset($_POST[$item])){
    //             $data[$item] = $_POST[$item];
    //         }
    //     }
    $update = new VehiculeRepository();
    $veh2 = $update->edit($_GET['id'], $immatriculation, $dateCirculation, $kilometrage, $dateCT, $sanitaire, $etat, $dateKM, $surnom);
    if($veh2){
        echo 'Modifié avec succès! ';
    }
}

?>

<div class="container mt-4">
    <h1>Editer le Véhicule</h1>

    <form action="/vehicules/edit?id=<?= $_GET['id'] ?>" name="editVehicule" method="POST">
    <?php  foreach($veh as $vehicule){ ?>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="vehicule_immatriculation">Immatriculation</label>
                    <input type="text" name="vehicule_immatriculation" id="vehicule_immatriculation" class="form-control" value="<?= $vehicule['immatriculation'] ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="vehicule_dateCirculation">Date de mise en circulation</label>
                    <input type="date" name="vehicule_dateCirculation" id="vehicule_dateCirculation" class="form-control" value="<?= $vehicule['dateCirculation'] ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="vehicule_kilometrage">Kilométrage au compteur</label>
                    <input type="number" name="vehicule_kilometrage" id="vehicule_kilometrage" class="form-control" value="<?= $vehicule['kilometrage'] ?>">
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="vehicule_dateCT">Date Contrôle technique</label>
                    <input type="date" name="vehicule_dateCT" id="vehicule_dateCT" class="form-control" value="<?= $vehicule['dateCT'] ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class="form-check">
                        <label class="form-check-label" for="oui">Sanitaire</label>
                        <input type="radio" name="sanitaire" id="oui" class="form-control" value="1" <?php if($vehicule['sanitaire'] == 1){ echo 'checked'; } ?>>
                        <label class="form-check-label" for="non">Non sanitaire</label>
                        <input type="radio" id="non" name="sanitaire" class="form-control" value="0"<?php if($vehicule['sanitaire'] == 0){ echo 'checked'; } ?>>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                <div class="form-check">
                        <label class="form-check-label" for="vendu">Vendu</label>
                        <input type="radio" name="etat" id="vendu" class="form-control" value="1" <?php if($vehicule['etat'] == 1){ echo 'checked'; } ?>>
                        <label class="form-check-label" for="parc">Parc</label>
                        <input type="radio" id="parc" name="etat" class="form-control" value="0"<?php if($vehicule['etat'] == 0){ echo 'checked'; } ?>>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="vehicule_dateKM">Date MAJ Kilométrage</label>
                    <input type="date" name="vehicule_dateKM" id="vehicule_dateKM" class="form-control" value="<?= $vehicule['dateKM'] ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class="form-check">
                        <label class="form-check-label" for="vehicule_surnom">Nom du véhicule</label>
                        <input type="text" name="vehicule_surnom" id="vehicule_surnom" class="form-control" value="<?= $vehicule['surnom'] ?>">
                    </div>
                </div>
            </div>
        </div>

        <button type="submit" name="goEdit" class="btn btn-primary">Modifier</button>
        <?php } ?>
    </form>


</div>


