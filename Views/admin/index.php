<div class="mt-4">
<h1>Bienvenue dans l'administration</h1>
</div>

<div class="row">
  <div class="col-sm-4">
<a class="btn btn-lg btn-success" href="/vehicules/index">
  <i class="fa fa-ambulance fa-2x pull-left"></i> Véhicules</a>
  </div>
  <div class="col-sm-4">
  <a class="btn btn-lg btn-warning" href="/entretiens/index">
  <i class="fa fa-files-o fa-2x pull-left"></i> Entretiens</a>
  </div>
  <?php if(isset($_SESSION['user']) && $_SESSION['user']['roles'] === 'admin'){ ?>
  <div class="col-sm-4">
   <!-- <a href="/users/register" class="btn btn-default"><i class="fa fa-user-circle fa-2x" aria-hidden="true"></i>  Gestion Utilisateurs</a> -->
   <a class="btn btn-lg btn-secondary" href="/users/register">
  <i class="fa fa-user-circle fa-2x pull-left"></i> Gestion Utilisateurs</a>
  </div>
  <?php } ?>
</div>