<!DOCTYPE html>
<html lang="en">
<head>
  <title>Live Search</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
  <h1>Rechercher l'entretien d'un véhicule dans la base de données</h1>
</div>
  
<div class="container">
  <div class="row">
    <div class="col-sm-3">
    </div>
    <div class="col-sm-6">
    <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-search fa-fw"></i></span>
                </div>
                <input type="text" class="form-control" id="search" placeholder="Rechercher un entretien" aria-label="Username" aria-describedby="basic-addon1" required>
            </div>
      <table class="table table-hover">
      <thead>
        <tr>
          <th>Intitulé</th>
          <th>Nom du Véhicule</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody id="output">
        
      </tbody>
    </table>
    </div>
    <div class="col-sm-3">
    <a href="/entretiens/add" class="btn btn-success"><i class="fa fa-plus"></i> Ajouter un entretien</a>
    </div>
  </div>
</div>
<script type="text/javascript">

function AJAX(){
  $.ajax({
        type:'POST',
        url: '/search/method',
        data:{
          name:$("#search").val(),
        },
        success:function(data){
          $("#output").html(data);
        }
      });
}

  $(document).ready(function(){
    AJAX();
    // let root = 'App\\Views\\vehicules\\';
    $("#search").keyup(function(){
      AJAX();
      
    });
  });

</script>


</body>
</html>
