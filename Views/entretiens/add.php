<div class="container mt-4">
    <h1>Ajouter un Véhicule</h1>

    <form action="/entretiens/insert/" method="POST">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="intitule">Intitulé de l'entretien</label>
                    <input type="text" name="intitule" id="intitule" class="form-control" value="">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="dateEntretien">Date de l'entretien</label>
                    <input type="date" name="dateEntretien" id="dateEntretien" class="form-control" value="">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="periodicite">Périodicité</label>
                    <input type="number" name="periodicite" id="periodicite" class="form-control" value="">
                </div>
            </div>
        </div>

<div class="row">
<div class="col-md-4">
                <div class="form-group">
                    <label for="periodicite">Véhicule Concerné</label>
                    <select name="vehicule_id" id="vehicule_id">
                   <?php foreach($vehicules as $item){  ?>
                        <option value="<?= $item['id'] ?>"><?= $item['surnom'] .' '. $item['immatriculation'].' '. $item['kilometrage'].' km'?></option>
                   <?php } ?>
                    </select>
                </div>
            </div>
    </div>
      
        <button type="submit" class="btn btn-primary">Ajouter</button>

    </form>



</div>
