<h1>Modifier un véhicule</h1>
<?php
use App\Database\DBConnection;
// Create connection
$pdo = new DBConnection();
        $res = $pdo->connect();

$sql = "SELECT surnom, immatriculation, kilometrage, id FROM vehicule";
$query = $res->prepare($sql);
            $query->execute();
            $row=$query->fetchAll();



foreach($entretiens as $entretien){
?>

<div class="container mt-4">

    <h1>Editer l'entretien concernant le véhicule : <?= $entretien['surnom'] ?> immatriculé  <?= $entretien['immatriculation'] ?></h1>

    <form action="/entretiens/update/" name="editEntretien" method="POST">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="intitule">Intitulé de l'entretien</label>
                    <input type="text" name="intitule" id="intitule" class="form-control" value="<?= $entretien['libelle'] ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="dateEntretien">Date de l'entretien</label>
                    <input type="date" name="dateEntretien" id="dateEntretien" class="form-control" value="<?= $entretien['dateEntretien'] ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="periodicite">Périodicité</label>
                    <input type="number" name="periodicite" id="periodicite" class="form-control" value="<?= $entretien['periodicite'] ?>">
                </div>
            </div>
        </div>
<?php } ?>
<div class="row">
<div class="col-md-4">
                <div class="form-group">
                    <label for="periodicite">Véhicule Concerné</label>
                    <select name="vehicule_id" id="vehicule_id">
                   <?php foreach($row as $item){  ?>
                        <option value="<?= $item['id'] ?>"><?= $item['surnom'] .' '. $item['immatriculation'].' '. $item['kilometrage'].' km'?></option>
                   <?php } ?>
                    </select>
                </div>
            </div>
    </div>
       
        <input type="hidden" name="id" value="<?= $entretien['id'] ?>">
        <button type="submit" name="updateentretien" class="btn btn-primary">Modifier</button>

    </form>


</div>
