<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <title>Service Transports CH Laon</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Inconsolata:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="app.css">
  </head>

<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
<a class="navbar-brand" href="#">
          <img src="../logoCH.png" width="110px" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="/">Parc auto</a>
            </li>
            <!-- <?php if(isset($_SESSION['user']) && !empty($_SESSION['user']['id'])): ?> -->
            <li class="nav-item">
              <a class="nav-link" href="/admin"><i class="fa fa-unlock"></i> Admin</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/controletechnique"><i class="fa fa-check-square-o"></i> Contrôle technique</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/archives"><i class="fa fa-archive"></i> Véhicules archivés</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/vehicules/kilometrage"><i class="fa fa-archive"></i> Kilométrage</a>
            </li>
            <!-- <?php endif; ?> -->
            <li class="nav-item">
              <?php if(isset($_SESSION['user']) && !empty($_SESSION['user']['id'])): ?>
              <a class="nav-link" href="/users/logout"><i class="fa fa-power-off"></i> Déconnexion</a>
                <?php else: ?>
                  <a class="nav-link" href="/users">Se connecter</a>
                <?php endif ?>
            </li>
            </ul>
        </div>
            <div class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <?php if(isset($_SESSION['user']) && !empty($_SESSION['user']['id'])): ?>
            <a class="nav-link text-success" href="/users/changepswd/<?= $_SESSION['user']['id'] ?>"><i class="fa fa-user-o"></i> Connecté en tant qu' <?= $_SESSION['user']['roles'] ?></a>
            <?php else: ?>
                  <a class="nav-link text-danger" href="">Déconnecté</a>
            <?php endif ?>
            </li>
            </ul>
            </div>
          
      </nav>
    <div class="container mt-4">
        <?php if(!empty($_SESSION['erreur'])): ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $_SESSION['erreur']; unset($_SESSION['erreur']); ?>
            </div>
        <?php endif; ?>
        <?php if(!empty($_SESSION['message'])): ?>
            <div class="alert alert-success" role="alert">
                <?php echo $_SESSION['message']; unset($_SESSION['message']); ?>
            </div>
        <?php endif; ?>
        <?= $contenu ?>
    </div>
    

    <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>

</html>