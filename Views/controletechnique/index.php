<!DOCTYPE html>
<html lang="en">
<head>
  <title>Live Search</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
  <h1>Rechercher les prochains contrôles technique</h1>
</div>

<div class="container">
  <div class="row">
    <!-- <div class="col-sm-3">
    </div> -->
    <div class="col-sm-9">
    <div class="form-group">
      <label for="">Année</label>
    <select class="form-control" name="" id="search" required>
    <option value="">Sélectionner l'année</option>
        <option value="2020">2020</option>
        <option value="2021">2021</option>
        <option value="2022">2022</option>
        <option value="2023">2023</option>
        <option value="2024">2024</option>
        <option value="2025">2025</option>
        <option value="2026">2026</option>
    </select>
    </div>
    <div class="form-group">
      <label for="">Mois</label>
    <select class="form-control" name="" id="searchByMonth" required>
    <option value="">Sélectionner le mois</option>    
    <option value="01">Janvier</option>
        <option value="02">Février</option>
        <option value="03">Mars</option>
        <option value="04">Avril</option>
        <option value="05">Mai</option>
        <option value="06">Juin</option>
        <option value="07">Juillet</option>
        <option value="08">Août</option>
        <option value="09">Septembre</option>
        <option value="10">Octobre</option>
        <option value="11">Novembre</option>
        <option value="12">Décembre</option>
    </select>
    </div>
      <table class="table table-hover">
      <thead>
        <tr>
          <th>Nom du véhicule</th>
          <th>Immatriculation</th>
          <th>Date Contrôle technique</th>
        </tr>
      </thead>
      <tbody id="output">
        
      </tbody>
    </table>
    </div>
    <div class="col-sm-3">
    </div>
  </div>
</div>
<script type="text/javascript">

function AJAX(){
  $.ajax({
        type:'POST',
        url: '/search/select',
        data:{
          name:$("#search").val(),
        },
        success:function(data){
          $("#output").html(data);
        }
      });
}
function AJAXbymonth(){
  $.ajax({
        type:'POST',
        url: '/search/select',
        data:{
            name:$('#search').val(),
          month:$("#searchByMonth").val(),
        },
        success:function(data){
          $("#output").html(data);
        }
      });
}
  $(document).ready(function(){
    AJAX();
    $("#search").change(function(){
      AJAX();
      
    });
          $("#searchByMonth").change(function(){
              AJAXbymonth();
          });
  });

</script>


</body>
</html>
