<?php
namespace App\Core;

use App\Controllers\MainController;


/**
 * Routeur principal
 */
class Router
{
    public function start()
    {
        // On démarre la session
        session_start();
           
            
        // On retire le "trailing slash" éventuel de l'URL
        // On récupère l'URL
        $uri = $_SERVER['REQUEST_URI'];
        

        // On vérifie que uri n'est pas vide et se termine par un /
        if(isset($uri[-1])){

            if(!empty($uri) && $uri != '/' && $uri[-1] === "/"){
                // On enlève le /
                // car $uri ne peut jamais être vide, il y a toujours au moins un '/'
                $uri = substr($uri, 0, -1);
    
                // On envoie un code de redirection permanente
                http_response_code(301);
    
                // On redirige vers l'URL sans /
                header('Location: '.$uri);
            }
        }

        // On gère les paramètres d'URL
        // p=controleur/methode/paramètres
        // On sépare les paramètres dans un tableau
        $params = [];
        if(isset($_GET['p']))
            $params = explode('/', $_GET['p']);

        if($params[0] != ''){
            // On a au moins 1 paramètre
            // On récupère le nom du contrôleur à instancier
            // On met une majuscule en 1ère lettre, on ajoute le namespace complet avant et on ajoute "Controller" après
            // ucfirst pour mettre 1ère lettre en majuscule
            // array_shift récupère la première valeur d'un tableau
            // on intégre le namespace complet pour pouvoir instancier directement le controller par la suite


            $controller = '\\App\\Controllers\\'.ucfirst(array_shift($params)).'Controller';
            
            

            // On instancie le contrôleur
            
                $controller = new $controller();
            
           
            // On récupère le 2ème paramètre d'URL
            // Si un paramètre existe on récupère le premier element du tableau et l'affecte à $action sinon $action = index
            // ceci permet de basculer directement sur les pages d'accueil des différentes sections du site
            $method = (isset($params[0])) ? array_shift($params) : 'index';
            
            
            if(method_exists($controller, $method)){
                // Si il reste des paramètres on les passe à la méthode
                // Si on a encore un paramètre on lance la méthode et on passe les paramètres sinon on lance la méthode
                
                (isset($params[0])) ? call_user_func_array([$controller, $method], $params) : $controller->$method();
            }else{
                http_response_code(404);
                echo "La page recherchée n'existe pas";
            }
            
        }else{
            // On n'a pas de paramètres
            // On instancie le contrôleur par défaut
            $controller = new MainController;
            
            // On appelle la méthode index
            $controller->index();
        }
    
    }
}